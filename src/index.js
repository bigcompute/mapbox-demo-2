import osmtogeojson from 'osmtogeojson'
import queryoverpass from 'query-overpass'


// var query_input="way["railway"="subway"]({{bbox}});"
// var test = queryoverpass(query_input,function(){
// request(this);
//
// })
//
// function request(output){
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    myFunction(this);
  }
};
xhttp.open("GET", "../data/osm_test.xml", true);
// xhttp.open("GET", ouput, true);
xhttp.send();

// }


function myFunction(xml) {

  mapboxgl.accessToken = 'pk.eyJ1IjoibW9vbnNjbyIsImEiOiJjamp6dHI0NGcyc2U2M2ttaDJ5ODdxYzZnIn0.a-DquIQvqYDqOrgBh5y7Aw';

  var tivoli = {
    lat: 55.67405678196548,
    lng: 12.568788199999972,
  };

  var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/light-v9',
    center: [tivoli.lng, tivoli.lat],
    zoom: 13
  });

  // Add zoom and rotation controls to the map.
  map.addControl(new mapboxgl.NavigationControl());

  var xmlDoc = xml.responseXML;
  var test2 = osmtogeojson(xmlDoc);

  var json_input={
      "id": "points",
      "type": "symbol",
      "source": {
          "type": "geojson",
          "data": {
            "type": "FeatureCollection",
            "features": [{}],
          },
        },
      "layout": {
          "icon-image": "{icon}-15",
          "text-field": "{title}",
          "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
          "text-offset": [0, 0.6],
          "text-anchor": "top"
      }
  };

 // json_input.source.data.features[0]=test2.features[0];
  json_input.source.data.features=test2.features;


console.log("about to enter loop")
console.log(test2)
  for (var i in json_input.source.data.features) {
      json_input.source.data.features[i].properties.title=test2.features[i].properties.name;
      json_input.source.data.features[i].properties.icon="monument";
  };
 // json_input.source.data.features[0].properties.title="Point1";
 // json_input.source.data.features[0].properties.icon="monument";


 console.log(json_input)


map.on('load', function() {
  map.addLayer(
    json_input
  )
  })

};
