# Multiple demos of Mapbox to test usability
1. demo1 = plotting building footprints in mapbox
2. demo2 = 3D buildings in mapbox
3. demo3 = importing XML, convert to GeoJSON, then plot points in mapbox
